export default function InputError({ message, className = '', ...props }) {
    return message ? (
        <span {...props} className={'text-sm text-red-600 mt-2 ' + className}>
            {message}
        </span>
    ) : null;
}
