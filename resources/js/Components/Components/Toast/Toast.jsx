import { Transition } from '@headlessui/react';
import { usePage } from '@inertiajs/react'

export default function Toast({}) {
    const { flash } = usePage().props;

    return (
                <Transition
                    show={ (flash?.success || flash?.error) ? true : false}
                    enter="ease-out duration-300"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="ease-in duration-200"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0">

                    <div className="max-w-xs bg-white border border-gray-200 rounded-xl shadow-lg absolute right-2 top-2" style={{zIndex: 999}} role="alert">
                        <div className="flex p-4">
                            <div className="flex-shrink-0">
                                <svg className="flex-shrink-0 size-4 text-teal-500 mt-0.5" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"></path>
                                </svg>
                            </div>
                            <div className="ms-3">
                                <p className="text-sm text-gray-700">
                                    {flash?.success && flash.success}
                                    {flash?.errror && flash.error}
                                </p>
                            </div>
                        </div>
                    </div>

                </Transition>

    )

}
