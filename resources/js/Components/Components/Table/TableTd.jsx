export default function TableTd({ children }) {
    return (
        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-800">{children}</td>
    )
}
