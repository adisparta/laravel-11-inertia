import Pagination from "../Pagination/Pagination";

export default function Table({headers, children, links = [], className = ""}) {
    return (
        <div className={`flex flex-col my-4 ${className}`}>
            <div className="-m-1.5 overflow-x-auto">
                <div className="p-1.5 min-w-full inline-block align-middle">
                    <div className="overflow-hidden">
                        <table className="min-w-full divide-y divide-gray-200">
                            <thead>
                                <tr>
                                    {headers.map((column, index) => (
                                        <th scope="col" className="px-6 py-3 text-start text-xs text-gray-500 uppercase font-bold" key={index}>{column}</th>
                                    ))}
                                </tr>
                            </thead>
                            <tbody className="divide-y divide-gray-200">
                                {children}
                            </tbody>
                        </table>
                    </div>
                    <Pagination links={links} />
                </div>
            </div>
        </div>
    )
}
