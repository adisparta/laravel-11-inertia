export default function TableTr({ children }) {

    return (
        <tr className="hover:bg-gray-100 ">
            {children}
        </tr>
    )

}
