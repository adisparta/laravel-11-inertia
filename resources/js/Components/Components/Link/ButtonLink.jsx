import { router } from "@inertiajs/react";
import { useState } from "react";

export default function ButtonLink({ children, className, href, ...props }) {

    const [loading, setLoading] = useState(false);

    const goToHref = (e) => {
        e.preventDefault();
        setLoading(true);
        router.visit(href);
    }

    return (
        <a onClick={goToHref} {...props} className={`w-full sm:w-auto py-3 px-4 inline-flex justify-center items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent text-blue-600 hover:text-blue-800 disabled:opacity-50 disabled:pointer-events-none ${className} relative overflow-hidden`} href={href}>
            {loading &&
                <div className="absolute left-0 right-0 top-0 bottom-0 flex items-center" style={{
                    background : "rgba(255, 255, 255, 0.6)"
                }}>
                    <span className="animate-spin mx-auto inline-block size-4 border-[3px] border-current border-t-transparent text-blue-600 rounded-full dark:text-blue-500" role="status" aria-label="loading">
                        <span className="sr-only">Loading...</span>
                    </span>
                </div>
            }
            {children}
        </a>
    )
}
