export default function ButtonPrimary({ className = "", loading, children, ...props }) {
	return (
      <button type="button" {...props} className={`py-3 px-5 inline-flex items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent bg-blue-600 text-white hover:bg-blue-700 disabled:opacity-50 overflow-hidden disabled:pointer-events-none relative mr-1 mb-1 ${className} ${loading ? "cursor-wait" : "cursor-pointer"}`}>
        {loading &&
            <div className="absolute left-0 right-0 top-0 bottom-0 flex items-center" style={{
                background : "rgba(0, 0, 0, 0.4)"
            }}>
                <span className="animate-spin mx-auto inline-block size-4 border-[3px] border-current border-t-transparent text-blue-600 rounded-full dark:text-blue-500" role="status" aria-label="loading">
                    <span className="sr-only">Loading...</span>
                </span>
            </div>
        }
        {children}
      </button>
	);
}
