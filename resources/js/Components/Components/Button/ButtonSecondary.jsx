export default function ButtonSecondary({ className = "",  loading = false, children, ...props }) {
	return (
      <button type="button" {...props} className={`py-3 px-5 relative overflow-hidden inline-flex items-center gap-x-2 text-sm font-medium rounded-lg border border-gray-200 bg-white text-gray-800 shadow-sm hover:bg-gray-50 disabled:opacity-50 mr-1 mb-1 disabled:pointer-events-noned ${className} ${loading ? "cursor-wait" : "cursor-pointer"}`} >
        {loading &&
            <div className="absolute left-0 right-0 top-0 bottom-0 flex items-center" style={{
                background : "rgba(255, 255, 255, 0.6)"
            }}>
                <span className="animate-spin mx-auto inline-block size-4 border-[3px] border-current border-t-transparent text-blue-600 rounded-full dark:text-blue-500" role="status" aria-label="loading">
                    <span className="sr-only">Loading...</span>
                </span>
            </div>
        }
        {children}
      </button>
	);
}
