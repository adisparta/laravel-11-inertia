export default function Card({ title = "", className="", children, ... props }) {

    return (
        <div className={`flex flex-col bg-white border shadow-sm rounded-xl p-4 md:p-5 mb-4 ${className}`} {...props}>
            {title != "" &&
                <h3 className="text-lg font-bold text-gray-800 mb-2">
                    {title}
                </h3>
            }
            <div className="mt-2 ">
                {children}
            </div>
        </div>
    )

}
