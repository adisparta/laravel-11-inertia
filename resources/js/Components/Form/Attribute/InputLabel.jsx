export default function InputLabel({ label, className, ...props }) {
    return (
        <label class={`block text-sm font-medium mb-2 ${className}`} {...props}>{label}</label>
    )
}
