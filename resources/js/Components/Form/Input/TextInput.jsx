import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import { forwardRef, useEffect, useRef } from 'react';

export default forwardRef(function TextInput({ type = 'text', label='', className = '', error = '', isFocused = false, optional = false, ...props }, ref) {
    const input = ref ? ref : useRef();

    useEffect(() => {
        if (isFocused) {
            input.current.focus();
        }
    }, []);

    return (
        <div className='my-4'>
        {label != "" &&
            <InputLabel optional={optional}>{label}</InputLabel>
        }
        <input
            {...props}
            type={type}
            className={
                `py-3 px-4 block w-full border-gray-200 rounded-lg text-sm disabled:opacity-50 disabled:pointer-events-none ${error == "" ? "focus:border-blue-500 focus:ring-blue-500" : "focus:border-red-500 focus:ring-red-500 border-red-500"} ` +
                className
            }
            ref={input}
        />
        { error != "" &&
            <InputError message={error} />
        }
        </div>
    );
});
