export default function InputLabel({ value, className = '', children, optional = false, ...props }) {
    return (
        <label {...props} className={`block text-sm font-medium mb-2 ` + className}>
            {value ? value : children}
            { optional &&
                <span className="ml-2 text-gray-400">(optional)</span>
            }
        </label>
    );
}
