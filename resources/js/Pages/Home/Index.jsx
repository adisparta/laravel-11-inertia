import ButtonPrimary from '@/Components/Components/Button/ButtonPrimary';
import ButtonSecondary from '@/Components/Components/Button/ButtonSecondary';
import Card from '@/Components/Components/Card/Card';
import Link from '@/Components/Components/Link/ButtonLink';
import Table from '@/Components/Components/Table/Table';
import TableTd from '@/Components/Components/Table/TableTd';
import TableTr from '@/Components/Components/Table/TableTr';
import TextInput from '@/Components/Form/Input/TextInput';
import DefaultLayout from '@/Layouts/DefaultLayout';
import React from 'react';

const Index = () => {
    const customHeaders = ['Name', 'Age', 'Country'];

    return (
        <DefaultLayout title={"Home Page"}>
            <Card title='Form Component'>

                <ButtonPrimary loading>Hello World</ButtonPrimary>
                <ButtonSecondary loading>Hello World</ButtonSecondary>
                <Link href={"/hello"}>Hello World</Link>
                <TextInput error="Please fill this form" label="Username" />
                <Table headers={customHeaders}>
                    <TableTr>
                        <TableTd>I Wayan</TableTd>
                        <TableTd>30</TableTd>
                        <TableTd>Indonesia</TableTd>
                    </TableTr>
                    <TableTr>
                        <TableTd>I Wayan</TableTd>
                        <TableTd>30</TableTd>
                        <TableTd>Indonesia</TableTd>
                    </TableTr>
                </Table>
            </Card>
        </DefaultLayout>
    )
}

export default Index
