import Card from "@/Components/Components/Card/Card"
import IconPlus from "@/Components/Components/Icons/IconPlus"
import Link from "@/Components/Components/Link/ButtonLink"
import Table from "@/Components/Components/Table/Table"
import TableTd from "@/Components/Components/Table/TableTd"
import TableTr from "@/Components/Components/Table/TableTr"
import DefaultLayout from "@/Layouts/DefaultLayout"

export default function Index({ users }) {
    return (
        <DefaultLayout title={"Users"} action={
            <Link href={route('users.create')}>
                <div className="flex  space-x-2 items-center">
                    <IconPlus className="text-blue-700 bg-blue-200" />
                    <div>Add New User</div>
                </div>
            </Link>
        }>

            <Card title="User List">
                <Table headers={["Name", "Email", "Role", "Created at"]} links={users.links}>
                    {users.data.map((user, index) => (
                        <TableTr key={index}>
                            <TableTd>{user.name}</TableTd>
                            <TableTd>{user.email}</TableTd>
                            <TableTd>{user.role}</TableTd>
                            <TableTd>{user.created_at}</TableTd>
                        </TableTr>
                    ))}
                </Table>
            </Card>

        </DefaultLayout>
    )
}
