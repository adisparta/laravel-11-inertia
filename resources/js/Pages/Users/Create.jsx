import ButtonPrimary from "@/Components/Components/Button/ButtonPrimary"
import Card from "@/Components/Components/Card/Card"
import IconPlus from "@/Components/Components/Icons/IconPlus"
import Link from "@/Components/Components/Link/ButtonLink"
import TextInput from "@/Components/Form/Input/TextInput"
import DefaultLayout from "@/Layouts/DefaultLayout"
import { router, usePage } from "@inertiajs/react"
import { useState } from "react"

export default function Create() {
    const {errors}                  = usePage().props
    const [isLoading, setIsLoading] = useState(false)
    const [formData, setFormData]   = useState({
        name    : "",
        email   : "",
        role    : "",
        password: ""
    });

    const doSubmit = (e) => {
        e.preventDefault();
        setIsLoading(true)
        router.post(route('users.store'), formData, {
            onSuccess: () => setIsLoading(false),
            onError  : () => setIsLoading(false)
        });
    }

    const handleChange = (e) => {
        const key = e.target.name;
        const value = e.target.value;
        setFormData(values => ({
            ...values,
            [key]: value,
        }))
    }

    return (
        <DefaultLayout title={"Users"}>

            <div className="grid lg:grid-cols-2">

                <Card title="Create New User">
                    <form onSubmit={doSubmit}>

                    <TextInput label="Full name" name="name" error={errors.name} onChange={handleChange} />
                    <TextInput label="Email" name="email" type="email" error={errors.email} onChange={handleChange} />
                    <TextInput label="Role" name="role" type="text" error={errors.role} onChange={handleChange} />
                    <TextInput type="password" label="Password" name="password" error={errors.password} onChange={handleChange} />

                    <ButtonPrimary type="submit" loading={ isLoading }>Save User</ButtonPrimary>

                    </form>
                </Card>

            </div>
        </DefaultLayout>
    )
}

